Simple Gallery

Make gallery dinamically with PHP.

Only remove all photos from directories ambum, album2 and album3 and add yours photos in this.
Create others directories album4, othername, etc and insert photos in this.

Create miniatures for each album in images folder add lines in index.html file for each album.

For each directory add copy index.php from other directory to it and change your title.

## Download
https://github.com/ribafs/simple-gallery

Screeshot

![Screenshot](/img/simplegallery.png)


**Português**

Criação de galerias de imagens dinâmicas com PHP.

Basta remover todas as imagens das pastas album, algum2 e album3 e adicionar as suas.
Crie mais pastas: album4, album5 ou outro nome qualquer, desde que referencie em index.html e adicione neles suas fotos.

Crie miniaturas para cada album na pasta images e adicione linhas respectivas no index.html.

Para cada novo diretório adicionado copie um index.php de outro diretório para ele e mude seu title e título no body.

Agradeço ao Supercain que compartilhou o código principal desta galeria em:
http://www.webdeveloper.com/forum/showthread.php?284153-Slideshow-that-automatically-shows-all-images-existing-in-a-given-folder
